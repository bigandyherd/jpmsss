#!/usr/bin/python3.5
# -*- coding: utf-8 -*-
""" run a series of tests on the Stock.record_trade fn"""

# Temporarily add the path of the other code in this project
# to the  path
import sys
from os.path import realpath
from pathlib import Path
root_path = Path(realpath(__file__)).parents[1].__str__()
sys.path.append('{}/jpmsss'.format(root_path))

import unittest
from hireme import Stock

 
class TestRecordTrade(unittest.TestCase):
    def setUp(self):
        self.stock = Stock(symbol='JPM', ticker_price=1,
                           last_dividend=100, par_value=1)
        self.trades_len_before = len(self.stock.trades)
        self.stock.record_trade(qty=12, buy_sell='buy', 
                                price=200, currency='GBP pennies')
 
    def test_rtn_type_trades(self):
        self.assertIsInstance(
            self.stock.trades, list
        )

    def test_rtn_type_one_trade(self):
        self.assertIsInstance(
            self.stock.trades[0], tuple
        )

    def test_trade_was_recorded(self):
        self.assertEqual(
            len(self.stock.trades), self.trades_len_before + 1
        )

    def test_correct_qty(self):
        self.assertEqual(
            self.stock.trades[0].qty, 12
        )

    def test_correct_category(self):
        self.assertEqual(
            self.stock.trades[0].buy_sell, 'buy'
        )

    def test_correct_price(self):
        self.assertEqual(
            self.stock.trades[0].price, 200
        )

    def test_correct_currency(self):
        self.assertEqual(
            self.stock.trades[0].currency, 'GBP pennies'
        )


if __name__ == '__main__':
    unittest.main()
