#!/usr/bin/python3.5
# -*- coding: utf-8 -*-
""" run a series of tests on the get_all_share_index fn"""

# Temporarily add the path of the other code in this project
# to the  path
import sys
from os.path import realpath
from pathlib import Path
root_path = Path(realpath(__file__)).parents[1].__str__()
sys.path.append('{}/jpmsss'.format(root_path))

import unittest
import hireme
from numpy import float64

"""
    I found a website which gave a couple of examples of
    the geometric mean calculation
    https://www.mathsisfun.com/numbers/geometric-mean.html

    The geometric mean of [2,18] should equal 6
    The geometric mean of [10, 51.2, 8] should equal 16
    Let's test!

    One of these tests produces a 15th decimal place, so I have rounded the results to 10dp
"""

class TestAllShareIndex(unittest.TestCase):
    def setUp(self):
        self.list1 = [
            hireme.Stock('test', 2, 100, 100),
            hireme.Stock('test', 18, 100, 100)
        ]
        self.list2 = [
            hireme.Stock('test', 10, 100, 100),
            hireme.Stock('test', 51.2, 100, 100),
            hireme.Stock('test', 8, 100, 100)
        ]
        self.result1 = hireme.get_all_share_index(self.list1)
        self.result2 = hireme.get_all_share_index(self.list2)
 
    def test_rtn_type(self):
        self.assertIsInstance(
            self.result1, float64
        )
 
    def test_list_1(self):
        self.assertEqual(
            round(self.result1,10), 6
        )

    def test_list_2(self):
        self.assertEqual(
            round(self.result2,10), 16
        )
 
    def test_type_error(self):
        """
        expect if we send a list of numbers that it would throw
        an error, because it expects a list of Stock objs
        """
        self.assertRaises(
            TypeError, hireme.get_all_share_index, [1, 2, 3]
        )
 
if __name__ == '__main__':
    unittest.main()