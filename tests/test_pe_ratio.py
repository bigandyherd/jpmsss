#!/usr/bin/python3.5
# -*- coding: utf-8 -*-
""" run a series of tests on the Stock.pe_ratio"""

# Temporarily add the path of the other code in this project
# to the  path
import sys
from os.path import realpath
from pathlib import Path
root_path = Path(realpath(__file__)).parents[1].__str__()
sys.path.append('{}/jpmsss'.format(root_path))

import unittest
from hireme import Stock

 
class TestPERatio(unittest.TestCase):
    """
        common stock POP
        expected PE ratio == 8 / 16 == 0.5
    """
    def setUp(self):
        self.stock = Stock(symbol='POP', ticker_price=16.0,
                           last_dividend=8.0, par_value=100)

    def test_rtn_type(self):
        self.assertIsInstance(
            self.stock.dividend_yield, float
        )

    def test_correct_value(self):
        self.assertEqual(
            self.stock.dividend_yield, 0.5
        )

 
if __name__ == '__main__':
    unittest.main()
