#!/usr/bin/python3.5
# -*- coding: utf-8 -*-
""" run a series of tests on the Stock.get_price"""

# Temporarily add the path of the other code in this project
# to the  path
import sys
from os.path import realpath
from pathlib import Path
root_path = Path(realpath(__file__)).parents[1].__str__()
sys.path.append('{}/jpmsss'.format(root_path))

import unittest
from hireme import Stock

 
class TestGetPrice(unittest.TestCase):
    """
        common stock POP
        let's record 3 trades
        expected value == 19620/40 == 490.5
    """
    def setUp(self):
        self.stock = Stock(symbol='POP', ticker_price=16.0,
                           last_dividend=8.0, par_value=100)
        self.stock.record_trade(1, 'buy', 100, 'GBP pennies')
        self.stock.record_trade(19, 'buy', 80, 'GBP pennies')
        self.stock.record_trade(20, 'sell', 900, 'GBP pennies')

    def test_rtn_type(self):
        self.assertIsInstance(
            self.stock.get_price(), float
        )

    def test_correct_value(self):
        self.assertEqual(
            self.stock.get_price(), 490.5
        )

 
if __name__ == '__main__':
    unittest.main()
