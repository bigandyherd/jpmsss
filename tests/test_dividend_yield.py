#!/usr/bin/python3.5
# -*- coding: utf-8 -*-
""" run a series of tests on the Stock.dividend_yield"""

# Temporarily add the path of the other code in this project
# to the  path
import sys
from os.path import realpath
from pathlib import Path
root_path = Path(realpath(__file__)).parents[1].__str__()
sys.path.append('{}/jpmsss'.format(root_path))

import unittest
from hireme import Stock

 
class TestCommonDividendYield(unittest.TestCase):
    """
        common stock POP, expected dividend yield == 0.5
    """
    def setUp(self):
        self.stock = Stock(symbol='POP', ticker_price=16.0,
                           last_dividend=8.0, par_value=100)

    def test_rtn_type(self):
        self.assertIsInstance(
            self.stock.dividend_yield, float
        )

    def test_correct_value(self):
        self.assertEqual(
            self.stock.dividend_yield, 0.5
        )


class TestPrefDividendYield(unittest.TestCase):
    """
        preferred stock GIN, expected dividend yield == 0.5
    """
    def setUp(self):
        self.stock = Stock(symbol='GIN', ticker_price=4.0,
                           last_dividend=8.0, par_value=100,
                           fixed_dividend=0.02)

    def test_rtn_type(self):
        self.assertIsInstance(
            self.stock.dividend_yield, float
        )

    def test_correct_value(self):
        self.assertEqual(
            self.stock.dividend_yield, 0.5
        )

 
if __name__ == '__main__':
    unittest.main()
