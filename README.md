# README

### JP Morgan Super Simple Stocks

This repository is a solution created by Andy Herd for the JP Morgan Super Simple Stocks challenge:

### Requirements

    1.	Provide working source code that will :-
        a.	For a given stock, 
            i.	calculate the dividend yield
            ii.	calculate the P/E Ratio
            iii.	record a trade, with timestamp, quantity of shares, buy or sell indicator and price
            iv.	Calculate Stock Price based on trades recorded in past 15 minutes
    b.	Calculate the GBCE All Share Index using the geometric mean of prices for all stocks

### How do I get set up?

Ensure that you are using Python3 (ideally 3.5). Create a clone of this repository by running the following: 

    git clone https://[your_bitbucket_username]@bitbucket.org/bigandyherd/jpmsss.git

There are some libraries referenced by this project. To install these, you can use pip to install them from the requirements file:

    pip install -r docs/requirements.txt


### Overview

#### hireme
`jpmsss/hireme.py`  contains all the code for the calculations

`Stock` is a class within hireme to represent a single stock

`Stock.dividend_yield` is a property to show the dividend yield (`1.a.i`)

`Stock.pe_ratio` is a property to show the price/earnings ratio (`1.a.ii`)

`Stock.record_trade` is a method to record a trade against this stock (`1.a.iii`)

`Stock.get_price` is a method to calculate the price based on trades within the last 15 minutes (`1.a.iv`)

`get_all_share_index` is a function to calculate the GBCE all share index (`1b`). It expects a list of `Stock`s.

#### Unit Tests
`tests` folder contains a test module for each facet of hireme. You can see an example the syntax for how to use each element of `hireme`.

From a cmd or terminal window with the root jpmsss folder as the current directory, you can use the following code to run all unit tests:

    python -m unittest discover tests
