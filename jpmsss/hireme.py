#!/usr/bin/python3.5
# -*- coding: utf-8 -*-
""" super simple stocks JP Morgan test """

from scipy.stats.mstats import gmean as geometricmean
from collections import namedtuple
import datetime

class Stock:
    """
        contains information for a stock
        & performs some calculations

        Args:
            symbol (str): stock symbol e.g. 'JPM' RQD
            ticker price (float): RQD
            last_dividend (float): RQD, GBP pennies
            par_value (float): RQD, GBP pennies
            fixed_dividend (float): only RQD if preferred share, percent

        Query: would this ever need used without last_dividend or par_value?
        judging by the sample data provided, the answer is no
        TO DO: clarify with the user then update specification

        TO DO: with more development I'd like to validate the input parameters
    """
    def __init__(self, symbol, ticker_price, last_dividend,
                 par_value, fixed_dividend=None):
        # TO DO: VALIDATION
        self.symbol = symbol
        self.ticker_price = ticker_price
        self.last_dividend = last_dividend
        self.par_value = par_value
        self.trades = []
        if fixed_dividend is None:
            # This must be a Common share
            self.type = 'Common'
            self.get_dividend_yield = self._get_common_dividend_yield
        else:
            # we have a fixed_dividend, so it's a Preferred share
            self.type = 'Preferred'
            self.fixed_dividend = fixed_dividend
            self.get_dividend_yield = self._get_pref_dividend_yield
        self.dividend_yield = self.get_dividend_yield()
        self.pe_ratio = self._get_price_earnings_ratio()

    def _get_common_dividend_yield(self):
        """only to be used for common shares"""
        return self.last_dividend / self.ticker_price

    def _get_pref_dividend_yield(self):
        """only to be used for preferred shares"""
        return (self.fixed_dividend * self.par_value) / self.ticker_price

    def _get_price_earnings_ratio(self):
        """
            calculate ratio of price to the company's earnings per share
        """
        return self.ticker_price / self.last_dividend

    def record_trade(self, qty, buy_sell, price, currency):
        """
            records a trade to the self.trades list

            Args:
                qty (int): the number of stocks being traded
                buy_sell (str): either 'buy' or 'sell'
                price (float): number to represent the price of the stocks
                currency (str): 3 letter currency code e.g. USD

            Returns:
                None

            TO DO: with more development I'd like to validate the input parameters
        """
        trade_template = namedtuple('Trade', ['symbol',
                                              'qty',
                                              'buy_sell',
                                              'price',
                                              'currency',
                                              'timestamp'])
        # use the UTC time in case this is used by a global audience
        tstamp = datetime.datetime.utcnow()
        trade = trade_template._make([self.symbol, qty, buy_sell, price, currency, tstamp])
        self.trades.append(trade)

    def _get_pe_ratio(self):
        """ placeholder"""
        dividend = self.dividend_yield * self.ticker_price
        return self.ticker_price / dividend

    def _get_recent_trades(self):
        """all of the trades from the last 15 minutes"""
        recent_trades = []
        cut_off_dtm = (datetime.datetime.utcnow() -
                       datetime.timedelta(minutes=15))
        for trade in self.trades:
            """
                if efficient use of a larger sample set was rqd,
                I would consider an alternative data storage container for
                self.trades, which would avoid having to loop through
                all trades each time.
                A pandas dataframe would be an ideal way to scale up.
            """
            if trade.timestamp >= cut_off_dtm:
                recent_trades.append(trade)
        return recent_trades

    def _update_ticker(self, price):
        """
            it seems logical that when the price is updated,
            the ticker price should also be updated.
            Just in case this is required, I've created a
            method to do so. However, this is not yet called from anywhere.

            Args:
                price (float): the new ticker_price

            Query:
                Is it correct to assume that price from trades in last 15 mins
                should replace the ticker_price? Logically, it seems like
                the history should also be factored in to this equation
                TO DO: clarify in User Acceptance Testing
        """
        self.ticker_price = price
        self.dividend_yield = self.get_dividend_yield()
        self.pe_ratio = self._get_pe_ratio()

    def get_price(self):
        """
            calculate the stock price based on trades in the last 15 mins
        """
        qty_traded = 0
        trade_costs = []
        for trade in self._get_recent_trades():
            qty_traded += trade.qty
            trade_costs.append(trade.price * trade.qty)
        total_costs = sum(trade_costs)
        price = total_costs / qty_traded
        # self._update_ticker(price)  # see _update_ticker docstring
        return price



def get_all_share_index(stocks):
    """
        calculates the GBCE all share index
        uses the geometric mean of prices for all stocks

        relies upon the excellent scipy library for the geometric mean calculation
        http://docs.scipy.org/doc/scipy-0.13.0/reference/generated/scipy.stats.mstats.gmean.html

        The acronym GBCE seems like it's been thrown in as a red herring
        so I'm gonna ignore it
        (though apparently there's a Great Big Craft Extravaganza in Ireland!)

        Args:
            stock (array-like): list of hireme.Stocks

        Returns:
            numpy.float64
    """
    prices = []
    for stock in stocks:
        if isinstance(stock, Stock) is False:
            raise TypeError('invalid stock found, expected list of hireme.Stock objects')
        else:
            prices.append(stock.ticker_price)
    return geometricmean(prices)
